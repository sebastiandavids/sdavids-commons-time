/*
 * Copyright (c) 2017, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.commons.time;

import static com.google.common.truth.Truth.assertThat;
import static io.sdavids.commons.test.MockServices.withServicesForRunnableInCurrentThread;
import static io.sdavids.commons.time.ClockSupplier.fixedClockSupplier;
import static io.sdavids.commons.time.ClockSupplier.fixedUtcClockSupplier;
import static io.sdavids.commons.time.ClockSupplier.systemDefaultZoneClockSupplier;
import static io.sdavids.commons.time.ClockSupplier.systemUtcClockSupplier;
import static java.time.Clock.systemDefaultZone;
import static java.time.Clock.systemUTC;
import static java.time.ZoneOffset.UTC;
import static java.util.ServiceLoader.load;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.generate;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.time.Clock;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Supplier;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junitpioneer.jupiter.DefaultTimeZone;

/*
  Run with VM options: -Dio.sdavids.commons.time.clock.supplier.default.cached=false
*/
@SuppressWarnings({"ClassCanBeStatic", "PMD.UseUtilityClass"})
class ClockSupplierTest {

  @DefaultTimeZone(EUROPE_BERLIN_ZONE_ID)
  @Nested
  class SystemUtcClockSupplier {

    @Test
    void returnsSystemUtcClockSupplier() throws InterruptedException {
      Supplier<Clock> supplier = systemUtcClockSupplier();

      ExecutorService executorService = newFixedThreadPool(5);

      List<Future<Clock>> result =
          executorService.invokeAll(
              generate(() -> (Callable<Clock>) supplier::get).limit(COUNT).collect(toList()));

      shutdownExecutorService(executorService);

      Set<Clock> clocks = result.stream().map(getFuture()).collect(toSet());

      assertThat(clocks).hasSize(1);
      assertThat(clocks).doesNotContain(null);

      Clock clock = clocks.iterator().next();

      assertThat(clock.getZone()).isEqualTo(systemUTC().getZone());

      Set<Instant> instants =
          generate(
                  supplier(
                      () -> {
                        MILLISECONDS.sleep(SLEEP_TIMEOUT_MS);
                        return clock.instant();
                      }))
              .limit(COUNT)
              .collect(toSet());

      assertThat(instants).hasSize((int) COUNT);
      assertThat(instants).doesNotContain(null);
    }

    @SuppressWarnings("unchecked")
    @Test
    void canBeSerialized() throws IOException, ClassNotFoundException {
      Supplier<Clock> original = systemUtcClockSupplier();
      Supplier<Clock> deserialized = assertSerializationAndDeserialization(original);

      Clock clock = deserialized.get();

      assertThat(clock.getZone().normalized()).isEqualTo(UTC_ZONE.normalized());
      assertThat(clock.instant()).isNotNull();
    }
  }

  @DefaultTimeZone(EUROPE_BERLIN_ZONE_ID)
  @Nested
  class SystemDefaultZoneClockSupplier {

    @Test
    void returnsSystemDefaultZoneClockSupplier() throws InterruptedException {
      Supplier<Clock> supplier = systemDefaultZoneClockSupplier();

      ExecutorService executorService = newFixedThreadPool(5);

      List<Future<Clock>> result =
          executorService.invokeAll(
              generate(() -> (Callable<Clock>) supplier::get).limit(COUNT).collect(toList()));

      shutdownExecutorService(executorService);

      Set<Clock> clocks = result.stream().map(getFuture()).collect(toSet());

      assertThat(clocks).hasSize(1);
      assertThat(clocks).doesNotContain(null);

      Clock clock = clocks.iterator().next();

      assertThat(clock.getZone()).isEqualTo(systemDefaultZone().getZone());

      Set<Instant> instants =
          generate(
                  supplier(
                      () -> {
                        MILLISECONDS.sleep(10L);
                        return clock.instant();
                      }))
              .limit(COUNT)
              .collect(toSet());

      assertThat(instants).hasSize((int) COUNT);
      assertThat(instants).doesNotContain(null);
    }

    @SuppressWarnings("unchecked")
    @Test
    void canBeSerialized() throws IOException, ClassNotFoundException {
      Supplier<Clock> original = systemDefaultZoneClockSupplier();
      Supplier<Clock> deserialized = assertSerializationAndDeserialization(original);

      Clock clock = deserialized.get();

      assertThat(clock.getZone()).isEqualTo(EUROPE_BERLIN_ZONE);
      assertThat(clock.instant()).isNotNull();
    }
  }

  @SuppressWarnings("ConstantConditions")
  @DefaultTimeZone(EUROPE_BERLIN_ZONE_ID)
  @Nested
  class FixedClockSupplier {

    @Test
    void withFixedInstantNull() {
      assertThrows(
          NullPointerException.class,
          () -> fixedClockSupplier(null, AMERICA_CHICAGO_ZONE),
          "fixedInstant");
    }

    @Test
    void withZoneNull() {
      assertThrows(
          NullPointerException.class, () -> fixedClockSupplier(FIXED_INSTANT, null), "zone");
    }

    @Test
    void returnsFixedClockSupplier() throws InterruptedException {
      Supplier<Clock> supplier = fixedClockSupplier(FIXED_INSTANT, AMERICA_CHICAGO_ZONE);

      ExecutorService executorService = newFixedThreadPool(5);

      List<Future<Clock>> result =
          executorService.invokeAll(
              generate(() -> (Callable<Clock>) supplier::get).limit(COUNT).collect(toList()));

      shutdownExecutorService(executorService);

      Set<Clock> clocks = result.stream().map(getFuture()).collect(toSet());

      assertThat(clocks).hasSize(1);
      assertThat(clocks).doesNotContain(null);

      Clock clock = clocks.iterator().next();

      assertThat(clock.getZone()).isEqualTo(AMERICA_CHICAGO_ZONE);

      Set<Instant> instants =
          generate(
                  supplier(
                      () -> {
                        MILLISECONDS.sleep(10L);
                        return clock.instant();
                      }))
              .limit(COUNT)
              .collect(toSet());

      assertThat(instants).hasSize(1);
      assertThat(instants).doesNotContain(null);

      assertThat(instants.iterator().next()).isEqualTo(FIXED_INSTANT);
    }

    @SuppressWarnings("unchecked")
    @Test
    void canBeSerialized() throws IOException, ClassNotFoundException {
      Supplier<Clock> original = fixedClockSupplier(FIXED_INSTANT, AMERICA_CHICAGO_ZONE);
      Supplier<Clock> deserialized = assertSerializationAndDeserialization(original);

      Clock clock = deserialized.get();

      assertThat(clock.getZone()).isEqualTo(AMERICA_CHICAGO_ZONE);
      assertThat(clock.instant()).isEqualTo(FIXED_INSTANT);
    }
  }

  @SuppressWarnings("ConstantConditions")
  @DefaultTimeZone(EUROPE_BERLIN_ZONE_ID)
  @Nested
  class FixedUtcClockSupplier {

    @Test
    void withFixedInstantNull() {
      assertThrows(NullPointerException.class, () -> fixedUtcClockSupplier(null), "fixedInstant");
    }

    @Test
    void returnsFixedUtcClockSupplier() throws InterruptedException {
      Supplier<Clock> supplier = fixedUtcClockSupplier(FIXED_INSTANT);

      ExecutorService executorService = newFixedThreadPool(5);

      List<Future<Clock>> result =
          executorService.invokeAll(
              generate(() -> (Callable<Clock>) supplier::get).limit(COUNT).collect(toList()));

      shutdownExecutorService(executorService);

      Set<Clock> clocks = result.stream().map(getFuture()).collect(toSet());

      assertThat(clocks).hasSize(1);
      assertThat(clocks).doesNotContain(null);

      Clock clock = clocks.iterator().next();

      assertThat(clock.getZone()).isEqualTo(UTC_ZONE);

      Set<Instant> instants =
          generate(
                  supplier(
                      () -> {
                        MILLISECONDS.sleep(10L);
                        return clock.instant();
                      }))
              .limit(COUNT)
              .collect(toSet());

      assertThat(instants).hasSize(1);
      assertThat(instants).doesNotContain(null);

      assertThat(instants.iterator().next()).isEqualTo(FIXED_INSTANT);
    }

    @SuppressWarnings("unchecked")
    @Test
    void canBeSerialized() throws IOException, ClassNotFoundException {
      Supplier<Clock> original = fixedUtcClockSupplier(FIXED_INSTANT);
      Supplier<Clock> deserialized = assertSerializationAndDeserialization(original);

      Clock clock = deserialized.get();

      assertThat(clock.getZone()).isEqualTo(UTC_ZONE);
      assertThat(clock.instant()).isEqualTo(FIXED_INSTANT);
    }
  }

  @DefaultTimeZone(EUROPE_BERLIN_ZONE_ID)
  @Nested
  class GetDefault {

    @RepeatedTest((int) COUNT)
    void returnsDefaultImpl() {
      Iterator<ClockSupplier> providers = load(ClockSupplier.class).iterator();

      assertThat(providers.hasNext()).isFalse();

      Supplier<Clock> first = ClockSupplier.getDefault();

      assertThat(first).isNotNull();

      Clock firstClock = first.get();

      assertThat(firstClock).isNotNull();
      assertThat(firstClock.instant()).isNotNull();
      assertThat(firstClock.getZone().normalized()).isEqualTo(UTC_ZONE.normalized());

      Supplier<Clock> second = ClockSupplier.getDefault();

      assertThat(second).isNotNull();

      Clock secondClock = second.get();

      assertThat(secondClock).isNotNull();
      assertThat(secondClock.instant()).isNotNull();
      assertThat(secondClock.getZone().normalized()).isEqualTo(UTC_ZONE.normalized());

      assertThat(first).isSameAs(second);
    }

    @RepeatedTest((int) COUNT)
    void returnsRegisteredImpl() {
      withServicesForRunnableInCurrentThread(
          () -> {
            Supplier<Clock> first = ClockSupplier.getDefault();

            assertThat(first).isNotNull();

            Clock firstClock = first.get();

            assertThat(firstClock).isNotNull();
            assertThat(firstClock.instant()).isEqualTo(FIXED_INSTANT);
            assertThat(firstClock.getZone()).isEqualTo(AMERICA_CHICAGO_ZONE);

            Supplier<Clock> second = ClockSupplier.getDefault();

            assertThat(second).isNotNull();

            Clock secondClock = second.get();

            assertThat(secondClock).isNotNull();
            assertThat(secondClock.instant()).isEqualTo(FIXED_INSTANT);
            assertThat(secondClock.getZone()).isEqualTo(AMERICA_CHICAGO_ZONE);

            assertThat(first).isSameAs(second);
          },
          TestableClockSupplier.class);
    }
  }

  @SuppressWarnings("PublicInnerClass")
  public static final class TestableClockSupplier extends ClockSupplier {

    @Override
    public Clock get() {
      return Clock.fixed(FIXED_INSTANT, AMERICA_CHICAGO_ZONE);
    }
  }

  static final String AMERICA_CHICAGO_ZONE_ID = "America/Chicago";
  static final String EUROPE_BERLIN_ZONE_ID = "Europe/Berlin";

  static final ZoneId AMERICA_CHICAGO_ZONE = ZoneId.of(AMERICA_CHICAGO_ZONE_ID);
  static final ZoneId EUROPE_BERLIN_ZONE = ZoneId.of(EUROPE_BERLIN_ZONE_ID);
  static final ZoneId UTC_ZONE = ZoneId.of("Etc/UTC");

  static final Instant FIXED_INSTANT = OffsetDateTime.of(2017, 10, 2, 17, 3, 0, 0, UTC).toInstant();

  // "the default interrupt source for timer and cyclic operations has a precision on the order of
  // 10 milliseconds"
  // https://blogs.oracle.com/jtc/real-time-java-and-high-resolution-timers
  // https://elinux.org/Kernel_Timer_Systems
  static final long SLEEP_TIMEOUT_MS = 10L;

  static final long COUNT = 1000L;

  @SuppressWarnings({"unchecked", "NoFunctionalReturnType"})
  static Supplier<Clock> assertSerializationAndDeserialization(Supplier<Clock> original)
      throws IOException, ClassNotFoundException {

    byte[] serialized;
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos)) {

      out.writeObject(original);

      serialized = bos.toByteArray();
    }

    try (ByteArrayInputStream bis = new ByteArrayInputStream(serialized);
        ObjectInputStream in = new ObjectInputStream(bis)) {

      return (Supplier<Clock>) in.readObject();
    }
  }

  @SuppressWarnings("NoFunctionalReturnType")
  static Function<Future<Clock>, Clock> getFuture() {
    return f -> {
      try {
        return f.get();
      } catch (InterruptedException | ExecutionException e) {
        throw new IllegalStateException(e);
      }
    };
  }

  @SuppressWarnings("NoFunctionalReturnType")
  static Supplier<Instant> supplier(Callable<Instant> supplier) {
    return () -> {
      try {
        return supplier.call();
      } catch (Exception e) {
        throw new IllegalStateException(e);
      }
    };
  }

  static void shutdownExecutorService(ExecutorService executorService) {
    executorService.shutdown();
    try {
      if (!executorService.awaitTermination(30L, SECONDS)) {
        executorService.shutdownNow();
      }
    } catch (InterruptedException e) {
      executorService.shutdownNow();
    }
  }
}
