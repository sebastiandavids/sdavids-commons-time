#!/usr/bin/env bash
echo -n "PGP Private Key Password: "
read -s gpgpassword
echo
./gradlew --no-daemon --no-build-cache --refresh-dependencies \
  -Prelease -Psigning.password="${gpgpassword}" \
  clean \
  build \
  publishToMavenLocal \
  bintrayUpload
